<div align="center"> <h1>A Hibernate-based API for your nerdy endeavours.</h1> 


![](misc/Vfx/Movie_Character_API2.png)

<p>
	<b>Storing/Manipulating your franchises, movies and their characters. With the possibility of adding other mediums.</b>

<br>
<br>
<br>
<br>

</div>  

## How does it work?  
The API is Hibernate-based and configured for use through PostgreSQL and Docker on your local machine. The data can be manipulated through SQL or Postman.

App runs locally on [localhost:8080](http://localhost:8080/).

[Postman Collection](https://gitlab.com/Nikodin/movie-characters-api/-/blob/development/misc/Movie%20Characters%20API.postman_collection.json) with the set of requests/saved formats.

[Swagger](http://localhost:8080/swagger-ui/index.html#/) contains the API requests/formats 
to manipulate the data _**when the application is running.**_

The database is seeded with a dummie-example, see notes below in the "Install/Optional"-section.

[Relationship Diagram](https://gitlab.com/Nikodin/movie-characters-api/-/blob/development/misc/API%20Diagram.jpg) contains the relationship of the generated schemas.

  
## Setup
* Install [PostgreSQL](https://www.postgresql.org/download/) and set up your local running instance for the API.
* Install [Git](https://git-scm.com)
* Install [Postman](https://www.postman.com/downloads/)

**Git Bash:**
```
docker pull postgres  
```
After a successful pull -
```
docker run --name {name of container} -p 5432/5432 -e POSTGRES_PASSWORD={your password} -d postgres
```
Check if the docker container is running in your Docker App.

##  Install:  
Clone Repository:
```
git clone https://gitlab.com/Nikodin/movie-characters-api.git 
```
Configure the application.properties, enter your own setup information to create a link to your local PostgreSQL instance.
![](misc/Vfx/properties.png)


### Optional (*Start empty/Non-demo-use*)
The Project is configured to seed template data (Harry Potter) into the database, if you want to keep the schemas but start fresh remove/move "data.sql"-file from the "resources"-folder  
and [nuke with sql](#nuking-demo-seeding).

**Edit the resource.properties-file and edit following if you go for a non-demo use:** 

![](misc/Vfx/propertiesconfignodemo.png)

## Usage:  
**Run the app through either:**
* Run app with Maven and running Jar through root folder:
```
maven build 
```
```
java -jar target/MovieCharacterAPI-0.0.1-SNAPSHOT.jar
```
* OR Run app through your IDE

## Postman & Swagger & SQL
The schemas can be manipulated in two ways: 

**Postman:**
* Import [Postman Collection](https://gitlab.com/Nikodin/movie-characters-api/-/blob/development/misc/Movie%20Characters%20API.postman_collection.json) from the "Misc"-folder to retrieve all used requests.
* See [Swagger](http://localhost:8080/swagger-ui/index.html#/) for documentation WHEN APP IS RUNNING 
  
**SQL:**
* Through SQL-queries in postgres or through IDE.


## Nuking Demo Seeding:
<details>
  <summary>SQL NUKE GENERATED SCHEMAS</summary>

  ```SQL
    DROP TABLE character CASCADE;
    DROP TABLE franchise CASCADE;
    DROP TABLE movie CASCADE;
    DROP TABLE movie_characters CASCADE;
  ```
</details>

## Credits:
[Nikodin](https://gitlab.com/Nikodin)  
[pauloavj](https://gitlab.com/pauloavj)

