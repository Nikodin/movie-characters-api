package com.example.alex.paulo.moviecharacterapi.repository;

import com.example.alex.paulo.moviecharacterapi.models.domain.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//Franchise Repo through Hibernate@
@Repository
public interface FranchiseRepo extends JpaRepository<Franchise, Long> {
}
