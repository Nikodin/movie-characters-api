package com.example.alex.paulo.moviecharacterapi.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;

//DTO for All Characters From Franchise-Report
@Getter
@Setter
@AllArgsConstructor
public class AllCharactersFromFranchiseDto {
    HashSet<String> listOfCharacters;
}
