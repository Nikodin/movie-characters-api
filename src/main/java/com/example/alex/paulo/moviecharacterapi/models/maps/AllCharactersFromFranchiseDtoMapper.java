package com.example.alex.paulo.moviecharacterapi.models.maps;

import com.example.alex.paulo.moviecharacterapi.models.domain.Character;
import com.example.alex.paulo.moviecharacterapi.models.domain.Franchise;
import com.example.alex.paulo.moviecharacterapi.models.domain.Movie;
import com.example.alex.paulo.moviecharacterapi.models.dto.AllCharactersFromFranchiseDto;
import org.springframework.stereotype.Component;

import java.util.HashSet;


@Component
public class AllCharactersFromFranchiseDtoMapper {
    /**This function requires a franchise object, and it will return a list of characters that belongs to the franchise*/
    public AllCharactersFromFranchiseDto getAllCharacters(Franchise franchise){
        HashSet<String> charactersList= new HashSet<>();
        var movies = franchise.getMoviesList();
        for (Movie movie: movies){
            for (Character character: movie.getCharacters()) {
                charactersList.add("/api/v1/characters/" + character.getId());
            }
        }
        return new AllCharactersFromFranchiseDto(charactersList);
    }
}
