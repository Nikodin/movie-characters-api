package com.example.alex.paulo.moviecharacterapi.models.domain;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Franchise {
    //Setting parameters for Franchise fields.
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 40, nullable = false)
    private String name;
    @Column(length = 1000, nullable = false)
    private String description;

    //Linked with Movies, owner side here.
    @OneToMany
    @JoinColumn(name = "movies_id")
    public List<Movie> moviesList;

    //Collects to list as url's.
    @JsonGetter("movies")
    public List<String> moviesList(){
        if(moviesList != null){
            return moviesList.stream()
                    .map(movie -> {
                        return "/api/v1/movies/" + movie.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }
}
