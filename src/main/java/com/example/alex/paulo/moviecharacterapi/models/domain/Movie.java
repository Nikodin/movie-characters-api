package com.example.alex.paulo.moviecharacterapi.models.domain;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Movie {
    //Setting parameters for Movie fields.
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 50, nullable = false)
    String title;
    @Column(length = 15, nullable = false)
    String genre;
    @Column(name = "release_year", nullable = false)
    LocalDate releaseYear;
    @Column(length = 50)
    String director;
    @Column(length = 80)
    String picture;
    @Column(length = 80)
    String trailer;

    //  Unidirectional Link with Characters
    @ManyToMany
    @JoinTable(
            name = "movie_characters",
            joinColumns = @JoinColumn(name = "movie_id"),
            inverseJoinColumns = @JoinColumn(name = "characters_id")
    )
    List<Character> characters;

    // Collects to list as url's
    @JsonGetter("characters")
    public List<String> charactersGetter(){
        if (characters != null){
            return characters.stream()
                    .map(character ->
                            "/api/v1/characters/" + character.getId())
                    .collect(Collectors.toList());
        }
        return null;
    }

    //Linked with Movies, not changable from this end.
    @ManyToOne
    @JoinColumn(name = "movies_id")
    public Franchise franchise;

    @JsonGetter("franchise")
    public String franchise(){
        if (franchise != null) {
            return "/api/v1/franchises/" + franchise.getId();
        } else {
            return null;
        }
    }
}
