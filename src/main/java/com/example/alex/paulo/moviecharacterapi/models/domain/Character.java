package com.example.alex.paulo.moviecharacterapi.models.domain;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Character {
    //Setting parameters for Character fields.
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "full_name", length = 40, nullable = false)
    String fullName;
    @Column(length = 100)
    String alias;
    @Column(length = 10)
    String gender;
    @Column(length = 80)
    String picture;

    //  Unidirectional Link with Movies
    @ManyToMany
    @JoinTable(
            name = "movie_characters",
            joinColumns = @JoinColumn(name = "characters_id"),
            inverseJoinColumns = @JoinColumn(name = "movie_id")
    )
    List<Movie> movies;

    //Collecting movies to list for the link as url's.
    @JsonGetter("movies")
    public List<String> moviesGetter(){
        if (movies != null){
            return movies.stream()
                    .map(movie -> "/api/v1/movie/" + movie.getId())
                    .collect(Collectors.toList());
        }
        return null;
    }
}
