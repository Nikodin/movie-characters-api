package com.example.alex.paulo.moviecharacterapi.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

//DTO for All Characters From Movie-Report
@Getter
@Setter
@AllArgsConstructor
public class AllCharactersFromMovieDto {
    List<String> listOfCharacters;
}
