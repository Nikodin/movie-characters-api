package com.example.alex.paulo.moviecharacterapi.models.maps;

import com.example.alex.paulo.moviecharacterapi.models.domain.Movie;
import com.example.alex.paulo.moviecharacterapi.models.dto.AllCharactersFromMovieDto;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AllCharactersFromMovieDtoMapper {
    /**This function requires a Movie object, and it will return a list of characters that belongs to the movie*/
    public AllCharactersFromMovieDto getAllCharacters(Movie movie){
        List<String> characters;
        characters = movie.charactersGetter();
        return new AllCharactersFromMovieDto(characters);
    }
}
