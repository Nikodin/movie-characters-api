package com.example.alex.paulo.moviecharacterapi.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

//DTO for All Movies From Franchise-Report
@Getter
@Setter
@AllArgsConstructor
public class AllMoviesFromFranchiseDto {
    List<String> listOfMovies;
}
