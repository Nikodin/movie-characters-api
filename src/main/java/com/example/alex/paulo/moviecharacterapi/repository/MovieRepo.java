package com.example.alex.paulo.moviecharacterapi.repository;

import com.example.alex.paulo.moviecharacterapi.models.domain.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//Movie Repo through Hibernate@
@Repository
public interface MovieRepo extends JpaRepository<Movie, Long> {

}
