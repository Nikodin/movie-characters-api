package com.example.alex.paulo.moviecharacterapi.repository;

import com.example.alex.paulo.moviecharacterapi.models.domain.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//Character Repo through Hibernate@
@Repository
public interface CharacterRepo extends JpaRepository<Character, Long> {
}
