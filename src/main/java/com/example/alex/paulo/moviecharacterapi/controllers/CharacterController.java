package com.example.alex.paulo.moviecharacterapi.controllers;

import com.example.alex.paulo.moviecharacterapi.models.domain.Character;
import com.example.alex.paulo.moviecharacterapi.repository.CharacterRepo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//Controller, CRUD for Characters, see Swagger-tags for details
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/characters")
@Tag(name = "Characters", description = "Contains Characters as: Long id, String fullName, String alias, String Gender, String picture and @ManyToMany movies relationship unidirectional")
public class CharacterController {

    private final CharacterRepo characterRepo;

    @Autowired
    public CharacterController(CharacterRepo characterRepo) {
        this.characterRepo = characterRepo;
    }

    @Operation(summary = "Get all Characters")
    @GetMapping()
    public ResponseEntity<List<Character>> getAllCharacters(){
        List<Character> characters = characterRepo.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(characters, status);
    }

    @Operation(summary = "Get a specific character using ID")
    @GetMapping("/{id}")
    public ResponseEntity<Character> getCharacter(@PathVariable Long id){
        Character character = new Character();
        HttpStatus status;
        //Checks if id exists
        if (characterRepo.existsById(id)) {
            status = HttpStatus.OK;
            character = characterRepo.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(character, status);
    }

    @Operation(summary = "Update specific Character using ID, and updated RequestBody")
    @PutMapping("/{id}")
    public ResponseEntity<Character> updateCharacter(@PathVariable Long id, @RequestBody Character character){
        Character updatedCharacter = new Character();
        HttpStatus status;
        //Check if ID exists
        if (!id.equals(character.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(updatedCharacter, status);
        }
        updatedCharacter = characterRepo.save(character);
        status = HttpStatus.ACCEPTED;
        return new ResponseEntity<>(updatedCharacter, status);
    }

    @Operation(summary = "Add a new Character to the database, matching RequestBody")
    @PostMapping
    public ResponseEntity<Character> addCharacter(@RequestBody Character character){
        Character newCharacter = characterRepo.save(character);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(newCharacter, status);
    }

    @Operation(summary = "Delete a specific Character using ID")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCharacter(@PathVariable Long id){
        if (!characterRepo.existsById(id)){
            return new ResponseEntity<>("No Character with that ID exists!", HttpStatus.NOT_FOUND);
        }
        characterRepo.deleteById(id);
        return new ResponseEntity<>("Character with ID: " + id + " has been deleted.", HttpStatus.OK);
    }
}
