package com.example.alex.paulo.moviecharacterapi;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@OpenAPIDefinition(
        info = @Info(
                title = "Movie Characters API",
                description = "Storing/Manipulating Franchises, Movies, and Characters"))
@SpringBootApplication
public class MovieCharacterApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MovieCharacterApiApplication.class, args);
    }

}
