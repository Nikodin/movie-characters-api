package com.example.alex.paulo.moviecharacterapi.models.maps;

import com.example.alex.paulo.moviecharacterapi.models.domain.Franchise;
import com.example.alex.paulo.moviecharacterapi.models.domain.Movie;
import com.example.alex.paulo.moviecharacterapi.models.dto.AllMoviesFromFranchiseDto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AllMoviesFromFranchiseDtoMapper {
    /**This function requires a franchise object, and it will return a list of movies that belongs to the franchise*/
    public AllMoviesFromFranchiseDto getAllMovies(Franchise franchise){
        List<String> moviesList= new ArrayList<>();
        var movies = franchise.getMoviesList();
        for (Movie movie: movies){
            moviesList.add("/api/v1/movie/" + movie.getId());
        }
        return new AllMoviesFromFranchiseDto(moviesList);
    }
}
