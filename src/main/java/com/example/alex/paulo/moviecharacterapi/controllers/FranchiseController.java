package com.example.alex.paulo.moviecharacterapi.controllers;

import com.example.alex.paulo.moviecharacterapi.models.domain.Franchise;
import com.example.alex.paulo.moviecharacterapi.models.dto.AllCharactersFromFranchiseDto;
import com.example.alex.paulo.moviecharacterapi.models.dto.AllMoviesFromFranchiseDto;
import com.example.alex.paulo.moviecharacterapi.models.maps.AllCharactersFromFranchiseDtoMapper;
import com.example.alex.paulo.moviecharacterapi.models.maps.AllMoviesFromFranchiseDtoMapper;
import com.example.alex.paulo.moviecharacterapi.repository.FranchiseRepo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//Controller, CRUD for Franchises, see Swagger-tags for details
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/franchises")
@Tag(name = "Franchises", description = "Contains Franchises as: Long id, String name, String description and @OneToMany movies relationship")
public class FranchiseController {

    FranchiseRepo franchiseRepo;
    AllMoviesFromFranchiseDtoMapper allMoviesFromFranchiseDtoMapper;
    AllCharactersFromFranchiseDtoMapper allCharactersFromFranchiseDtoMapper;

    @Autowired
    public FranchiseController(FranchiseRepo franchiseRepo, AllMoviesFromFranchiseDtoMapper allMoviesFromFranchiseDtoMapper, AllCharactersFromFranchiseDtoMapper allCharactersFromFranchiseDtoMapper) {
        this.franchiseRepo = franchiseRepo;
        this.allMoviesFromFranchiseDtoMapper = allMoviesFromFranchiseDtoMapper;
        this.allCharactersFromFranchiseDtoMapper = allCharactersFromFranchiseDtoMapper;
    }

    @Operation(summary = "Get all Franchises")
    @GetMapping()
    public ResponseEntity<List<Franchise>> getAllFranchises(){
        List<Franchise> franchises = franchiseRepo.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(franchises, status);
    }

    @Operation(summary = "Get a specific Franchise using ID")
    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getFranchise(@PathVariable Long id){
        Franchise franchise = new Franchise();
        HttpStatus status;
        //Checks if id exists
        if (franchiseRepo.existsById(id)) {
            status = HttpStatus.OK;
            franchise = franchiseRepo.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(franchise, status);
    }

    @Operation(summary = "Update specific Franchise using ID, and updated RequestBody")
    @PutMapping("/{id}")
    public ResponseEntity<Franchise> updateFranchise(@PathVariable Long id, @RequestBody Franchise franchise){
        Franchise updatedFranchise = new Franchise();
        HttpStatus status;
        //Check if ID exists
        if (!id.equals(franchise.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(updatedFranchise, status);
        }
        updatedFranchise = franchiseRepo.save(franchise);
        status = HttpStatus.ACCEPTED;
        return new ResponseEntity<>(updatedFranchise, status);
    }

    @Operation(summary = "Update a specific franchise set of movies, using Franchise ID and ID's for Movies as RequestBody")
    @PatchMapping("/{id}")
    public ResponseEntity<Franchise>updateSetOfMovies(@RequestBody Franchise setOfMovies, @PathVariable Long id){
        try {
            Franchise franchise = franchiseRepo.getById(id);
            franchise.setMoviesList(setOfMovies.getMoviesList());
            return new ResponseEntity<>(franchiseRepo.save(franchise), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Operation(summary = "Add a new Franchise to the database, matching RequestBody")
    @PostMapping
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise){
        Franchise newFranchise = franchiseRepo.save(franchise);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(newFranchise, status);
    }

    @Operation(summary = "Delete a specific Franchise using ID")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteFranchise(@PathVariable Long id){
        if (!franchiseRepo.existsById(id)){
            return new ResponseEntity<>("No Franchise with that ID exists!", HttpStatus.NOT_FOUND);
        }
        franchiseRepo.deleteById(id);
        return new ResponseEntity<>("Franchise with ID: " + id + " has been deleted", HttpStatus.OK);
    }

    @Operation(summary = "Get all movies linked to a specific franchise using ID")
    @GetMapping("/movies/{id}")
    public ResponseEntity<AllMoviesFromFranchiseDto> allMoviesFromFranchise(@PathVariable Long id){
        if (franchiseRepo.existsById(id)){
            var returnList = allMoviesFromFranchiseDtoMapper.getAllMovies(franchiseRepo.getById(id));
            return new ResponseEntity<>(returnList, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @Operation(summary = "Get all characters linked to a specific franchise using ID")
    @GetMapping("/characters/{id}")
    public ResponseEntity<AllCharactersFromFranchiseDto> allCharactersFromFranchise(@PathVariable Long id){
        if (franchiseRepo.existsById(id)){
            var returnList = allCharactersFromFranchiseDtoMapper.getAllCharacters(franchiseRepo.getById(id));
            return new ResponseEntity<>(returnList, HttpStatus.OK);
        }
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
}

