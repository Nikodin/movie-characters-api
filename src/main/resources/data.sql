--Characters entries
INSERT INTO character (id, alias, full_name, gender, picture)
VALUES 
(1,'The Dark Lord, Lord Voldemort, You-Know-Who', 'Tom Riddle', 'Male', 'tinyurl.com/2p85upph' ),
(2,'The Chosen One', 'Harry Potter', 'Male', 'https://tinyurl.com/2bdthxjk'),
(3,'', 'Albus Dumbledore', 'Male', 'https://tinyurl.com/4mumfdpf'),
(4,'', 'Hermoine Granger', 'Female', 'https://tinyurl.com/yer6ex6b'),
(5,'', 'Ron Weasley', 'Male', 'https://tinyurl.com/53fuk6wx' ),
(6,'', 'Bellatrix Lestrange', 'Female', 'https://tinyurl.com/2n7xnwnv'),
(7, 'Strider, Elessar, King of Gondor','Aragorn', 'Male', 'https://tinyurl.com/5n9astn5');

--Franchise entries, Harry Potter, LOTR
INSERT INTO franchise (id, description, name)
VALUES (1,'Based on the novels by J.K Rowling the series of Harry Potter consists of seven novels and eight films taking place in the wizarding world!', 'Harry Potter'),
 (2, 'Brought to the world by J.R.R Tolkien, the LOTR franchise has taken the world by storm consisting of Tolkiens written works as well as the movie adapations', 'Lord of the Rings');

--Movie entries, all HP movies
INSERT INTO movie (id, director, genre, picture, release_year, title, trailer, movies_id)
VALUES 
(1,'Chris Columbus', 'Fantasy', 'https://tinyurl.com/2p8bmj6b', '2001-11-16', 'Harry Potter and the Philosopher''s Stone', 'https://www.youtube.com/watch?v=VyHV0BRtdxo', 1),
(2,'Chris Columbus', 'Fantasy', 'https://tinyurl.com/3s3uys6k', '2002-11-14', 'Harry Potter and the Chamber of Secrets', 'https://www.youtube.com/watch?v=s4Fh2WQ2Xbk', 1),
(3,'Alfonso Cuarón', 'Fantasy', 'https://tinyurl.com/yc3eyv9m', '2004-05-31', 'Harry Potter and the Prisoner of Azkaban', 'https://www.youtube.com/watch?v=OgLy1LgSdjM', 1),
(4,'Mike Newell', 'Fantasy', 'https://tinyurl.com/5n6kjd64', '2005-11-18', 'Harry Potter and the Goblet of Fire', 'https://www.youtube.com/watch?v=iw0RWnC28Gs', 1),
(5,'David Yates', 'Fantasy', 'https://tinyurl.com/2jm73azn', '2007-07-11', 'Harry Potter and the Order of the Pheonix', 'https://www.youtube.com/watch?v=vz2_xS4TN6w', 1),
(6, 'Peter Jackson', 'Fantasy','https://tinyurl.com/csv9xarj', '2001-12-10', 'Lord of the Rings: Fellowship of the Ring', 'https://www.youtube.com/watch?v=_nZdmwHrcnw', 2);

--For ManyToMany between Chars/Movie Movie id to Char id
INSERT INTO movie_characters (characters_id, movie_id)
VALUES
(1, 1),
(1, 2),
(1, 4),
(1, 5),
(2, 1),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(3, 1),
(3, 2),
(3, 3),
(3, 4),
(3, 5),
(4, 1),
(4, 2),
(4, 3),
(4, 4),
(4, 5),
(5, 1),
(5, 2),
(5, 3),
(5, 4),
(5, 5),
(6, 5),
(7, 6);






